defmodule DeliveryCenter.Orders.CreateTest do
  use DeliveryCenter.DataCase

  alias DeliveryCenter.Order
  alias DeliveryCenter.Orders.Create

  describe "call/1" do
    test "when all params are valid, returns an order" do
      params = ~s({
        "city": "Cidade de Testes",
        "complement": "teste",
        "country": "BR",
        "customer": {
            "contact": "41999999999",
            "email": "john@doe.com",
            "externalCode": 136226073,
            "name": "JOHN DOE"
        },
        "deliveryFee": 5.14,
        "district": "Vila de Testes",
        "dtOrderCreate": "2019-06-24T16:45:32.000-04:00",
        "externalCode": 9987071,
        "items": [
            {
                "externalCode": "IT4801901403",
                "name": "Produto de Testes",
                "price": 49.9,
                "quantity": 1,
                "subItems": [],
                "total": 49.9
            }
        ],
        "latitude": -23.629037,
        "longitude": -46.712689,
        "number": "3454",
        "payments": [
            {
                "type": "credit_card",
                "value": 55.04
            }
        ],
        "postalCode": "85045020",
        "state": "São Paulo",
        "storeId": 282,
        "street": "Rua Fake de Testes",
        "subTotal": 49.9,
        "total": 55.04,
        "total_shipping": 5.14
      })
      |> Jason.decode!

      {:ok, %{create_order: order_created}} = Create.call(params)
      %DeliveryCenter.Order{id: order_id} = order_created

      order = Repo.get(Order, order_id)
      assert %Order{externalCode: 9987071, id: ^order_id} = order
    end

    test "when required fields are missing, returns an error" do
      params = ~s({"city": "Cidade de Testes"}) |> Jason.decode!
      {:error, _operation, changeset, _changes} = Create.call(params)

      expected_response = %{complement: ["can't be blank"], country: ["can't be blank"], deliveryFee: ["can't be blank"], district: ["can't be blank"], dtOrderCreate: ["can't be blank"], externalCode: ["can't be blank"], latitude: ["can't be blank"], longitude: ["can't be blank"], number: ["can't be blank"], postalCode: ["can't be blank"], state: ["can't be blank"], storeId: ["can't be blank"], street: ["can't be blank"], subTotal: ["can't be blank"], total: ["can't be blank"]}

      assert errors_on(changeset) == expected_response
    end
  end
end
