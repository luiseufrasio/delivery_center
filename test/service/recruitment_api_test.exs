defmodule Service.RecruitmentApiTest do
  use ExUnit.Case, async: true

  alias Service.RecruitmentApi

  describe "post/1" do
    test "sending params with success" do
      params = ~s({
        "city": "Cidade de Testes",
        "complement": "teste",
        "country": "BR",
        "customer": {
            "contact": "41999999999",
            "email": "john@doe.com",
            "externalCode": 136226073,
            "name": "JOHN DOE"
        },
        "deliveryFee": 5.14,
        "district": "Vila de Testes",
        "dtOrderCreate": "2019-06-24T16:45:32.000-04:00",
        "externalCode": 9987071,
        "items": [
            {
                "externalCode": "IT4801901403",
                "name": "Produto de Testes",
                "price": 49.9,
                "quantity": 1,
                "subItems": [],
                "total": 49.9
            }
        ],
        "latitude": -23.629037,
        "longitude": -46.712689,
        "number": "3454",
        "payments": [
            {
                "type": "credit_card",
                "value": 55.04
            }
        ],
        "postalCode": "85045020",
        "state": "São Paulo",
        "storeId": 282,
        "street": "Rua Fake de Testes",
        "subTotal": 49.9,
        "total": 55.04,
        "total_shipping": 5.14
      })

      {:ok, response, _body} =  RecruitmentApi.post(params)
      %HTTPoison.Response{body: body} = response

      assert body == "OK"
    end

    test "sending invalid params" do
      params = ~s({
        "city": "Cidade de Testes",
        "complement": "teste",
        "country": "BR",
        "customer": {
            "contact": "41999999999",
            "email": "john@doe.com",
            "externalCode": 136226073,
            "name": "JOHN DOE"
        },
        "deliveryFee": 5.14,
        "district": "Vila de Testes",
        "dtOrderCreate": "2019-06-24T16:45:32.000-04:00",
        "externalCode": 9987071,
        "items": [
            {
                "externalCode": "IT4801901403",
                "name": "Produto de Testes",
                "price": 49.9,
                "quantity": 1,
                "subItems": [],
                "total": 49.9
            }
        ],
        "latitude": -23.629037,
        "longitude": -46.712689,
        "number": "3454",
        "payments": [
            {
                "type": "credit_card",
                "value": 55.04
            }
        ],
        "postalCode": "85045020",
        "state": "São Paulo",
        "storeId": 282,
        "street": "Rua Fake de Testes",
        "subTotal": 49.9,
        "total": 55.04
      })

      {:ok, response, _body} =  RecruitmentApi.post(params)
      %HTTPoison.Response{body: body} = response

      assert body == "[total_shipping] are required"
    end
  end
end
