defmodule DeliveryCenterWeb.PageControllerTest do
  use DeliveryCenterWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to Delivery Center Test Dev BE!"
  end
end
