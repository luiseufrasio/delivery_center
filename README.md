# DeliveryCenter

To start the database in docker:

  * sudo docker start "ID"

Run migration script:

  * mix ecto.migrate

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit: 
[`localhost:4000/payload/Payload.json`](http://localhost:4000/payload/Payload.json) from your browser.

this will start the process...

Notice that this URL ends with the filename.

Try typing a file that does not exists: "_Payload.json" (for instance)

This and other cenarios are covered (=~ 80%) in the Tests. Try these commands:

  * mix test
  * mix coveralls.html

I've created just one controller (PayloadController) to handle the request
and orchestrate the process by delegating the features to the correspondent module:

  * DeliveryCenter.Payload.ParseFile => has the functions to handle the file
  * Service.RecruitmentApi => client to the external API
  * DeliveryCenter.Orders.Create => has the functions to persist the data into Postgres

Any doubts about this code? Please contact me:

  * luis.eufrasio@gmail.com