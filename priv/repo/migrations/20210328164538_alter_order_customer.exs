defmodule DeliveryCenter.Repo.Migrations.AlterOrderCustomer do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      remove :customer_id, references(:customers), null: false
    end

    alter table(:customers) do
      add :order_id, references(:orders), null: false
    end
  end
end
