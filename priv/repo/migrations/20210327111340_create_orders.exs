defmodule DeliveryCenter.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :externalCode, :integer, null: false
      add :storeId, :integer, null: false
      add :subTotal, :decimal, null: false
      add :deliveryFee, :decimal, null: false
      add :total, :decimal, null: false
      add :country, :string, null: false
      add :state, :string, null: false
      add :city, :string, null: false
      add :district, :string, null: false
      add :street, :string, null: false
      add :complement, :string, null: false
      add :latitude, :decimal, null: false
      add :longitude, :decimal, null: false
      add :dtOrderCreate, :naive_datetime, null: false
      add :postalCode, :string, null: false
      add :number, :string, null: false

      timestamps()
    end
  end
end
