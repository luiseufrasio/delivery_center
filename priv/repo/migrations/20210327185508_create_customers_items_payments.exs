defmodule DeliveryCenter.Repo.Migrations.CreateCustomersItemsPayments do
  use Ecto.Migration

  def change do
    create table(:customers) do
      add :contact, :string, null: false
      add :email, :string, null: false
      add :externalCode, :integer, null: false
      add :name, :string, null: false

      timestamps()
    end

    create table(:items) do
      add :externalCode, :string, null: false
      add :name, :string, null: false
      add :price, :decimal, null: false
      add :quantity, :integer, null: false
      add :total, :decimal, null: false
      add :super_item_id, references(:items)
      add :order_id, references(:orders), null: false

      timestamps()
    end

    create table(:payments) do
      add :type, :string, null: false
      add :value, :decimal, null: false
      add :order_id, references(:orders), null: false

      timestamps()
    end

  end
end
