defmodule DeliveryCenter.Repo.Migrations.RemoveTimestamps do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      remove :inserted_at
      remove :updated_at
    end

    alter table(:customers) do
      remove :inserted_at
      remove :updated_at
    end

    alter table(:items) do
      remove :inserted_at
      remove :updated_at
    end

    alter table(:payments) do
      remove :inserted_at
      remove :updated_at
    end
  end
end
