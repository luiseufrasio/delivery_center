defmodule DeliveryCenter.Orders.Create do
  alias Ecto.Multi
  alias DeliveryCenter.Repo
  alias DeliveryCenter.{Order, Customer, Item, Payment}

  def call(params) do
    Multi.new()
    |> Multi.insert(:create_order, Order.changeset(params))
    |> Multi.run(:create_customer, fn repo, %{create_order: order} ->
      insert_customer(repo, params["customer"], order)
    end)
    |> Multi.insert_all(:insert_all_items, Item, fn %{create_order: order} ->
      params["items"]
      |> Item.changesets([], order.id)
    end)
    |> Multi.insert_all(:insert_all_payments, Payment, fn %{create_order: order} ->
      params["payments"]
      |> Payment.changesets([], order.id)
    end)
    |> Repo.transaction()
  end

  def insert_customer(repo, params, order) do
    params
    |> Map.put("order_id", order.id)
    |> Customer.changeset()
    |> repo.insert()
  end
end
