defmodule DeliveryCenter.Customer do
  use Ecto.Schema
  import Ecto.Changeset

  alias DeliveryCenter.Order

  @required_params [:contact, :email, :externalCode, :name, :order_id]

  schema "customers" do
    field :contact, :string
    field :email, :string
    field :externalCode, :integer
    field :name, :string
    belongs_to :order, Order
  end

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, @required_params)
    |> validate_required(@required_params)
  end
end
