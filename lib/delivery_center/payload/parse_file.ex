defmodule DeliveryCenter.Payload.ParseFile do

  def parse(file) do
    file
    |> Jason.decode!
    |> map_fields
    |> Jason.encode!
  end

  defp map_fields(json) do
    %{
      externalCode: json["id"],
      storeId: json["store_id"],
      subTotal: json["total_amount"],
      deliveryFee: json["total_shipping"],
      total: json["paid_amount"],
      dtOrderCreate: json["date_created"],
      total_shipping: json["total_shipping"]
    }
    |> map_localization(json["shipping"])
    |> map_customer(json["buyer"])
    |> map_items([], json["order_items"])
    |> map_payments([], json["payments"])
  end

  defp map_localization(map, shipping_json) do
    Map.put(map, :country, shipping_json["receiver_address"]["country"]["id"])
    |> Map.put(:state, shipping_json["receiver_address"]["state"]["name"])
    |> Map.put(:city, shipping_json["receiver_address"]["city"]["name"])
    |> Map.put(:district, shipping_json["receiver_address"]["neighborhood"]["name"])
    |> Map.put(:street, shipping_json["receiver_address"]["street_name"])
    |> Map.put(:complement, shipping_json["receiver_address"]["comment"])
    |> Map.put(:latitude, shipping_json["receiver_address"]["latitude"])
    |> Map.put(:longitude, shipping_json["receiver_address"]["longitude"])
    |> Map.put(:number, shipping_json["receiver_address"]["street_number"])
    |> Map.put(:postalCode, shipping_json["receiver_address"]["zip_code"])
  end

  defp map_customer(map, buyer_json) do
    area_code = "#{buyer_json["phone"]["area_code"]}"
    phone_number = "#{buyer_json["phone"]["number"]}"
    Map.put(map, :customer, %{
      externalCode: buyer_json["id"],
      name: buyer_json["nickname"],
      email: buyer_json["email"],
      contact: area_code <> phone_number
    })
  end

  defp map_items(map, items_array, [head | tail]) do
    items_array = items_array ++ [%{
      externalCode: head["item"]["id"],
      name: head["item"]["title"],
      price: head["unit_price"],
      quantity: head["quantity"],
      total: head["full_unit_price"],
      subItems: []
    }]
    map_items(map, items_array, tail)
  end
  defp map_items(map, items_array, []), do: Map.put(map, :items, items_array)

  defp map_payments(map, payments_array, [head | tail]) do
    payments_array = payments_array ++ [%{
      type: head["payment_type"],
      value: head["total_paid_amount"]
    }]
    map_payments(map, payments_array, tail)
  end
  defp map_payments(map, payments_array, []), do: Map.put(map, :payments, payments_array)
end
