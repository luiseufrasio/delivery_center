defmodule DeliveryCenter.Item do
  use Ecto.Schema
  import Ecto.Changeset

  alias DeliveryCenter.{Order, Item}

  @required_params [:externalCode, :name, :price, :quantity, :total, :order_id]

  schema "items" do
    field :externalCode, :string
    field :name, :string
    field :price, :decimal
    field :quantity, :integer
    field :total, :decimal
    has_one :super_item, Item
    belongs_to :order, Order
  end

  def changeset(attrs) do
    changeset = cast(%__MODULE__{}, attrs, @required_params) |> validate_required(@required_params)
    changeset.changes
  end

  def changesets([head | tail], changeset_list, order_id) do
    changeset_list = changeset_list ++ [
      head
      |> Map.put("order_id", order_id)
      |> changeset
    ]
    changesets(tail, changeset_list, order_id)
  end
  def changesets([], changeset_list, _order_id), do: changeset_list
end
