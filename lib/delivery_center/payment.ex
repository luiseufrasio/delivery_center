defmodule DeliveryCenter.Payment do
  use Ecto.Schema
  import Ecto.Changeset

  alias DeliveryCenter.Order

  @required_params [:type, :value, :order_id]

  schema "payments" do
    field :type, :string
    field :value, :decimal
    belongs_to :order, Order
  end

  def changeset(attrs) do
    changeset = cast(%__MODULE__{}, attrs, @required_params) |> validate_required(@required_params)
    changeset.changes
  end

  def changesets([head | tail], changeset_list, order_id) do
    changeset_list = changeset_list ++ [
      head
      |> Map.put("order_id", order_id)
      |> changeset
    ]
    changesets(tail, changeset_list, order_id)
  end
  def changesets([], changeset_list, _order_id), do: changeset_list
end
