defmodule DeliveryCenter.Order do
  use Ecto.Schema
  import Ecto.Changeset

  alias DeliveryCenter.{Customer, Item, Payment}

  @required_params [:externalCode, :storeId, :subTotal, :deliveryFee, :total, :country, :state, :city, :district, :street, :complement, :latitude, :longitude, :dtOrderCreate, :postalCode, :number]

  schema "orders" do
    field :externalCode, :integer
    field :storeId, :integer
    field :subTotal, :decimal
    field :deliveryFee, :decimal
    field :total, :decimal
    field :country, :string
    field :state, :string
    field :city, :string
    field :district, :string
    field :street, :string
    field :complement, :string
    field :latitude, :decimal
    field :longitude, :decimal
    field :dtOrderCreate, :naive_datetime
    field :postalCode, :string
    field :number, :string
    has_one :customer, Customer
    has_many :items, Item
    has_many :payments, Payment
  end

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, @required_params)
    |> validate_required(@required_params)
  end
end
