defmodule DeliveryCenterWeb.PageController do
  use DeliveryCenterWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
