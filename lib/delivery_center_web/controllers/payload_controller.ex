defmodule DeliveryCenterWeb.PayloadController do
  use DeliveryCenterWeb, :controller

  alias DeliveryCenter.Payload.ParseFile
  alias Service.RecruitmentApi

  def index(conn, %{"filename" => filename}) do
    parsed_file_result = filename
    |> File.read
    |> handle_read_file

    render(conn, "payload.html", [filename: filename, parsed_file: parsed_file_result])
  end

  defp handle_read_file({:ok, file}) do
    file
    |> ParseFile.parse
    |> RecruitmentApi.post
    |> handle_response
  end
  defp handle_read_file({:error, :enoent}), do: ~s({error: "File not found!"})

  defp handle_response({:ok, %HTTPoison.Response{status_code: 200, body: _body}, parsed_file}) do
    parsed_file
    |> Jason.decode!
    |> DeliveryCenter.create_order
    parsed_file
  end
  defp handle_response({:ok, %HTTPoison.Response{status_code: 400, body: body}, _parsed_file}), do: ~s({code: 400, error: "#{body}"})
  defp handle_response({:ok, %HTTPoison.Response{status_code: 500, body: body}, _parsed_file}), do: ~s({code: 500, error: "#{body}"})
  defp handle_response({:error, %HTTPoison.Error{reason: reason}, _parsed_file}), do: ~s({error: "#{reason}"})

end
