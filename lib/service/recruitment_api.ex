defmodule Service.RecruitmentApi do
  @url "https://delivery-center-recruitment-ap.herokuapp.com/"

  def post(parsed_file) do
    @url
    |> HTTPoison.post(parsed_file, [{"X-Sent", current_datetime_formatted()}], [{"Content-Type", "application/json"}])
    |> Tuple.append(parsed_file)
  end

  defp current_datetime_formatted() do
    today = Timex.local()
    ~s(#{today.hour}h#{today.minute} - #{today.day}/#{today.month}/#{today.year})
  end
end
